const q = require('q');
const express = require('express');
const Post = require('../services/post');
const Service = require('../services/service');

const router = express.Router();


router.get('/', global.cache.route(), (req, res, next) => {
	let services = [];
	let posts = [];
	
	q.all([
		Service.list().then(srv => services = srv),
		Post.list().then(pst => posts = pst)
	]).then(() =>
		res.render('index', {
			services: services,
			posts: posts
		})
	).catch(reason =>
		res.render('index', {
			error: reason
		})
	);
});

module.exports = router;
