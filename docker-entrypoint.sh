#!/bin/bash

HOST_DOMAIN="host.docker.internal"
echo "> Checking if '$HOST_DOMAIN' is set"
ping -q -c1 $HOST_DOMAIN > /dev/null 2>&1
if [ $? -ne 0 ]; then
	HOST_IP=$(ip route | awk 'NR==1 {print $3}')
	echo -e "$HOST_IP\t$HOST_DOMAIN" >> /etc/hosts
	echo "> Manually set '$HOST_DOMAIN' as workaround"
fi

echo "> Starting server"
node server.js
