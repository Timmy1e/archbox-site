FROM node:latest

# Create app directory, and move to it
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app/

# Set Node to production
ENV NODE_ENV=development

# Install all deps
RUN npm install

# Open port for server
EXPOSE 3000

# Set start command for server, through workaround
CMD [ "bash", "docker-entrypoint.sh" ]
