const q = require('q');
const MysqlService = require('./mysqlService');
const PostLink = require('./postLink');

class Post {

	constructor(postObj = {}) {
		this.id = postObj.id || null;
		this.title = postObj.title || null;
		this.text = postObj.text || null;
		this.createdOn = global.momentNull(postObj.createdOn) || global.momentNull(postObj.created_on) || null;
		this.createdBy = postObj.createdBy || postObj.created_by || null;
		this.updatedOn = global.momentNull(postObj.updatedOn) || global.momentNull(postObj.updated_on) || null;
		this.updatedBy = postObj.updatedBy || postObj.updated_by || null;
	}

	static list() {
		const defer = q.defer();
		let connection;
		let posts = [];

		MysqlService.open()
			.then(newConnection => {
				connection = newConnection;
				return MysqlService.execute(
					connection,
					'SELECT p.* ' +
					'FROM `post` p ' +
					'ORDER BY p.`created_on` DESC ' +
					'LIMIT 20;',
					[]
				);
			})
			.then(postResult => {
				posts = postResult.map(post => new Post(post));

				return q.all(posts.map(post =>
					MysqlService.execute(
						connection,
						'SELECT l.* ' +
						'FROM `post_link` l ' +
						'WHERE l.`post_id` = ? ' +
						'ORDER BY l.`order` ASC;',
						[post.id]
					).then(linkResult =>
						post.links = linkResult.map(link =>
							new PostLink(link)
						)
					)
				));
			})
			.then(() => {
				connection.release();
				return defer.resolve(posts);
			})
			.catch(reason => {
				connection.release();
				defer.reject(reason);
			});

		return defer.promise;
	}

	static detail(id) {
		const defer = q.defer();
		let connection;
		let post;

		MysqlService.open()
			.then(newConnection => {
				connection = newConnection;
				return MysqlService.execute(
					connection,
					'SELECT p.* ' +
					'FROM `post` p ' +
					'WHERE p.`id` = ? ' +
					'LIMIT 1;',
					[id]
				);
			})
			.then(result => {
				if (result.length !== 1) {
					return defer.reject({message: 'Not found.'});
				}
				post = new Post(result[0]);
				return MysqlService.execute(
					connection,
					'SELECT l.* ' +
					'FROM `post_link` l ' +
					'WHERE l.`post_id` = ? ' +
					'ORDER BY l.`order` ASC;',
					[post.id]
				);
			})
			.then(linkResult => {
				connection.release();
				post.links = linkResult.map(link =>
					new PostLink(link)
				);
				defer.resolve(post);
			})
			.catch(reason => {
				connection.release();
				defer.reject(reason);
			});

		return defer.promise;
	}
}

module.exports = Post;