const q = require('q');
const MysqlService = require('./mysqlService');
const ServiceLink = require('./serviceLink');

class Service {

	constructor(serviceObj = {}) {
		this.id = serviceObj.id || null;
		this.name = serviceObj.name || null;
		this.text = serviceObj.text || null;
		this.active = serviceObj.active || null;
		this.createdOn = global.momentNull(serviceObj.createdOn) || global.momentNull(serviceObj.created_on) || null;
		this.createdBy = serviceObj.createdBy || serviceObj.created_by || null;
		this.updatedOn = global.momentNull(serviceObj.updatedOn) || global.momentNull(serviceObj.updated_on) || null;
		this.updatedBy = serviceObj.updatedBy || serviceObj.updated_by || null;
	}

	static list() {
		const defer = q.defer();
		let connection;
		let services = [];

		MysqlService.open()
			.then(newConnection => {
				connection = newConnection;
				return MysqlService.execute(
					connection,
					'SELECT s.* ' +
					'FROM `service` s ' +
					'WHERE s.`active` = TRUE ' +
					'LIMIT 20;',
					[]
				);
			})
			.then(serviceResult => {
				services = serviceResult.map(service => new Service(service));
				return q.all(
					services.map(service =>
						MysqlService.execute(
							connection,
							'SELECT l.* ' +
							'FROM `service_link` l ' +
							'WHERE l.`service_id` = ? ' +
							'ORDER BY l.`order` ASC;',
							[service.id]
						).then(linkResult =>
							service.links = linkResult.map(link =>
								new ServiceLink(link)
							)
						)
					)
				);
			}).then(() => {
			connection.release();
			return defer.resolve(services);
		})
			.catch(reason => {
				connection.release();
				defer.reject(reason);
			});

		return defer.promise;
	}

	static detail(id) {
		const defer = q.defer();
		let connection;
		let service;

		MysqlService.open()
			.then(newConnection => {
				connection = newConnection;
				return MysqlService.execute(
					connection,
					'SELECT s.* ' +
					'FROM `service` s ' +
					'WHERE s.`id` = ? ' +
					'LIMIT 1;',
					[id]
				);
			})
			.then(result => {
				if (result.length !== 1) {
					return defer.reject({message: 'Not found.'});
				}
				service = new Service(result[0]);
				return MysqlService.execute(
					connection,
					'SELECT l.* ' +
					'FROM `service_link` l ' +
					'WHERE l.`service_id` = ? ' +
					'ORDER BY l.`order` ASC;',
					[service.id]
				);
			})
			.then(linkResult => {
				connection.release();
				service.links = linkResult.map(link =>
					new ServiceLink(link)
				);
				defer.resolve(service);
			})
			.catch(reason => {
				connection.release();
				defer.reject(reason);
			});

		return defer.promise;
	}
}

module.exports = Service;