class PostLink {
	constructor(serviceLinkObj = {}) {
		this.id = serviceLinkObj.id || null;
		this.text = serviceLinkObj.text || null;
		this.url = serviceLinkObj.url || null;
		this.order = serviceLinkObj.order || null;
		this.postId = serviceLinkObj.postId || serviceLinkObj.post_id || null;
		this.createdOn = global.momentNull(serviceLinkObj.createdOn) || global.momentNull(serviceLinkObj.created_on) || null;
		this.createdBy = serviceLinkObj.createdBy || serviceLinkObj.created_by || null;
		this.updatedOn = global.momentNull(serviceLinkObj.updatedOn) || global.momentNull(serviceLinkObj.updated_on) || null;
		this.updatedBy = serviceLinkObj.updatedBy || serviceLinkObj.updated_by || null;
	}
}

module.exports = PostLink;
