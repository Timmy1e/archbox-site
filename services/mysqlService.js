const q = require('q');

class MysqlService {
	
	static open() {
		const defer = q.defer();
		
		global.pool.getConnection((poolError, connection) => {
			if (poolError) {
				defer.reject(poolError);
			} else {
				defer.resolve(connection);
			}
		});
		
		return defer.promise;
	}
	
	static execute(connection, queryString, queryVars) {
		const defer = q.defer();
		
		connection.query(queryString, queryVars, (queryError, queryResults) => {
				if (queryError) {
					defer.reject(queryError);
				} else {
					defer.resolve(queryResults);
				}
			});
		
		return defer.promise;
	}
}

module.exports = MysqlService;
