const path = require('path');
const redis = require('express-redis-cache');
const mysql = require('mysql');
const logger = require('morgan');
const moment = require('moment');
const express = require('express');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const sassMiddleware = require('node-sass-middleware');
const babelMiddleware = require('express-babel').default;

const app = express();


/// Moment
global.momentNull = value => value ? moment(value) : null;


/// Redis cache
global.cache = redis({
	prefix: 'xyz.archbox',
	host: process.env.x_archbox_redis_host, // jshint ignore:line
	expire: process.env.x_archbox_redis_expire ? parseInt(process.env.x_archbox_redis_expire) : undefined // jshint ignore:line
});


/// Mysql
global.pool = mysql.createPool({
	host: process.env.x_archbox_database_host, // jshint ignore:line
	user: process.env.x_archbox_database_user, // jshint ignore:line
	password: process.env.x_archbox_database_password, // jshint ignore:line
	database: process.env.x_archbox_database_database // jshint ignore:line
});


/// View engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


/// Rest
app.use(favicon(path.join(__dirname, 'public', 'images', 'ArchBox_logo.ico')));
app.use(logger(global.isDevelopment ? 'dev' : 'tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(sassMiddleware({
	src: path.join(__dirname, 'public'),
	dest: path.join(__dirname, 'public'),
	indentedSyntax: false,
	outputStyle: 'compressed',
	sourceMap: false
}));
app.use('/js', babelMiddleware(path.join(__dirname, 'public', 'js'), {
	compact: true,
	minified: true,
	presets: ['env', 'minify']
}));
app.use(express.static(path.join(__dirname, 'public')));

// Dependencies
addStaticDep('angular');
addStaticDep('angular-animate');
addStaticDep('bootstrap', path.join('bootstrap', 'dist'));
addStaticDep('jquery', path.join('jquery', 'dist'));
addStaticDep('popper', path.join('popper.js', 'dist', 'umd'));
addStaticDep('moment', path.join('moment', 'min'));
addStaticDep('font-awesome');

function addStaticDep(depName, depPath = depName) {
	app.use('/dist/' + depName, express.static(path.join(__dirname, 'node_modules', depPath)));
}

// /Dependencies

app.use('/robots.txt', express.static(path.join(__dirname, 'views', 'robots.txt')));


app.use('/', require('./routes/index'));


// catch 404 and forward to error handler
app.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use((err, req, res, next) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = global.isDevelopment ? err : {};

	// render the error page
	res.status(err.HttpStatusCode || err.status || 500);
	res.render('error');
});

module.exports = app;
