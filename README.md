[![pipeline status](https://gitlab.com/Timmy1e/archbox-site/badges/master/pipeline.svg)](https://gitlab.com/Timmy1e/archbox-site/commits/master)
[![coverage report](https://gitlab.com/Timmy1e/archbox-site/badges/master/coverage.svg)](https://gitlab.com/Timmy1e/archbox-site/commits/master)

# ArchBox.xyz website
This is the [ArchBox.xyz website](https://archbox.xyz) NodeJS Docker project.

This is automatically build using the GitLab CI and pushed to the [Docker Hub repo](https://cloud.docker.com/repository/docker/timmy1e/archbox/).
